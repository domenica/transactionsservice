package code.challenge.transaction.service;

import code.challenge.transaction.model.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Transaction store.
 */
public class TransactionStore {

    private List<Transaction> transactions = new ArrayList<>();

    /**
     * Returns an unmodifiable list of all the transactions.
     * @return list of all transactions
     */
    public List<Transaction> getTransactions() {
        return Collections.unmodifiableList(transactions);
    }

    /**
     * Adds a transaction.
     * @param transaction the transaction
     * @return true if add successful.
     */
    public boolean addTransaction(Transaction transaction) {
        return transactions.add(transaction);
    }

    /**
     * Deletes all the transactions.
     */
    public void removeAllTransactions() {
        transactions.clear();
    }

    private static TransactionStore transactionStore;

    public static TransactionStore getInstance() {
        if (transactionStore == null) {
            transactionStore = new TransactionStore();
        }
        return transactionStore;
    }
    private TransactionStore() {
    }
}

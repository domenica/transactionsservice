package code.challenge.transaction.service;

import code.challenge.transaction.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Transaction service.
 */
@Service
public class TransactionService {

    TransactionStore transactionStore = TransactionStore.getInstance();

    /**
     * Adds a transaction.
     * @param transaction the transaction
     * @return true if add successful.
     */
    public boolean save(Transaction transaction) {
        return transactionStore.addTransaction(transaction);
    }

    /**
     * Returns the transaction by id.
     * @param id the transaction id
     * @return found transaction
     */
    public Transaction getTransaction(Long id) {
        return transactionStore.getTransactions().stream().filter(t -> id.equals(t.getId())).findFirst().orElse(null);
    }

    /**
     * Finds all transactions which have given type.
     * @param type the transaction type
     * @return all transactions of given type
     */
    public List<Long> getTransactionsByType(String type) {
        return transactionStore.getTransactions()
                .stream().filter(t -> type.equals(t.getType())).map(Transaction::getId).collect(Collectors.toList());
    }

    /**
     * Calculates the sum of all transactions that are transitively linked by their parentId to transactionId.
     * @param id the id of the transaction we need the sum for
     * @return the sum
     */
    public double sumForTransaction(Long id) {
        Transaction transaction = getTransaction(id);
        if (transaction == null) {
            return 0;
        }
        double sum = transaction.getAmount();
        List<Transaction> childTransactions = transactionStore.getTransactions()
                .stream().filter(t -> id.equals(t.getParentId())).collect(Collectors.toList());
        if (childTransactions.isEmpty()) {
            return sum;
        } else {
            for (Transaction childTransaction: childTransactions) {
                sum += sumForTransaction(childTransaction.getId());
            }
            return sum;
        }
    }
}

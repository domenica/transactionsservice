package code.challenge.transaction.controllers;

import code.challenge.transaction.model.Transaction;
import code.challenge.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Transaction controller.
 */
@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    /**
     * Stores a transaction.
     * @param transactionId the transaction id
     * @param transaction the transaction data to be stored
     */
    @RequestMapping(value = "/transactionservice/transaction/{transactionId}", method = PUT,
            headers = {"content-type=application/json"})
    @ResponseStatus(value = HttpStatus.OK)
    public void save(@PathVariable("transactionId") Long transactionId, @RequestBody Transaction transaction) {
        transaction.setId(transactionId);
        transactionService.save(transaction);
    }

    /**
     * Returns the transaction by id.
     * @param transactionId the transaction id
     * @return found transaction
     */
    @RequestMapping(value = "/transactionservice/transaction/{transactionId}", method = GET)
    public Transaction getTransaction(@PathVariable("transactionId") Long transactionId) {
        return transactionService.getTransaction(transactionId);
    }

    /**
     * Finds all transactions which have given type.
     * @param type the transaction type
     * @return all transactions of given type
     */
    @RequestMapping(value = "/transactionservice/types/{type}", method = GET)
    public List<Long> getTransactionsByType(@PathVariable("type") String type) {
        return transactionService.getTransactionsByType(type);
    }

    /**
     * Calculates the sum of all transactions that are transitively linked by their parentId to transactionId.
     * @param transactionId the transaction we need the sum for
     * @return the sum
     */
    @RequestMapping(value = "/transactionservice/sum/{transactionId}", method = GET)
    public double sumForTransaction(@PathVariable("transactionId") Long transactionId) {
        return transactionService.sumForTransaction(transactionId);
    }
}
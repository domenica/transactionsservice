package code.challenge.transaction.controllers;

import code.challenge.transaction.model.Transaction;
import code.challenge.transaction.service.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Test for TransactionController.
 */
@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    @InjectMocks
    private TransactionController transactionController;

    @Mock
    private TransactionService transactionService;

    @Test
    public void shouldSave() throws Exception {
        //given

        //when
        Transaction transaction = new Transaction();
        transaction.setAmount(100);
        transactionController.save(1L, transaction);

        //then
        verify(transactionService).save(transaction);
    }

    @Test
    public void shouldGetTransaction() throws Exception {
        //given

        //when
        transactionController.getTransaction(1L);

        //then
        verify(transactionService).getTransaction(1L);
    }

    @Test
    public void shouldGetTransactionsByType() throws Exception {
        //given

        //when
        transactionController.getTransactionsByType("cars");

        //then
        verify(transactionService).getTransactionsByType("cars");
    }

    @Test
    public void shouldSumForTransaction() throws Exception {
        //given

        //when
        transactionController.sumForTransaction(1L);

        //then
        verify(transactionService).sumForTransaction(1L);
    }
}
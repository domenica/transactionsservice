package code.challenge.transaction.service;

import code.challenge.transaction.model.Transaction;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test for TransactionService.
 */
public class TransactionServiceTest {

    TransactionService transactionService = new TransactionService();
    TransactionStore transactionStore = TransactionStore.getInstance();

    @Before
    public void setupTransactions() {
        transactionStore.removeAllTransactions();
        Transaction transaction = new Transaction(1L, 100, "cars", null);
        transactionStore.addTransaction(transaction);
        transaction = new Transaction(2L, 50, "shopping", null);
        transactionStore.addTransaction(transaction);
        transaction = new Transaction(3L, 200, "cars", 1L);
        transactionStore.addTransaction(transaction);
        transaction = new Transaction(4L, 350, "cars", 1L);
        transactionStore.addTransaction(transaction);
    }

    @Test
    public void shouldSave() throws Exception {
        // given
        Transaction transaction = new Transaction(5L, 800, "computers", null);
        int sizeBeforeAdd = transactionStore.getTransactions().size();

        // when
        boolean saved = transactionService.save(transaction);

        // then
        assertTrue(saved);
        assertThat(transactionStore.getTransactions().size(), Matchers.equalTo(sizeBeforeAdd + 1));
    }

    @Test
    public void shouldGetTransaction() throws Exception {
        // given

        // when
        Transaction transaction = transactionService.getTransaction(3L);

        // then
        assertThat(transaction.getId(), Matchers.equalTo(3L));
        assertThat(transaction.getAmount(), Matchers.equalTo(200.0));
        assertThat(transaction.getType(), Matchers.equalTo("cars"));
        assertThat(transaction.getParentId(), Matchers.equalTo(1L));
    }

    @Test
    public void shouldGetTransactionsByType() throws Exception {
        // given

        // when
        List<Long> transactions = transactionService.getTransactionsByType("cars");

        // then
        assertThat(transactions.size(), Matchers.equalTo(3));
    }

    @Test
    public void shouldSumWhenNoChildren() throws Exception {
        // given

        // when
        double sum = transactionService.sumForTransaction(3L);

        // then
        assertThat(sum, Matchers.equalTo(200.0));
    }

    @Test
    public void shouldSumWithChildren() throws Exception {
        // given

        // when
        double sum = transactionService.sumForTransaction(1L);

        // then
        assertThat(sum, Matchers.equalTo(650.0));
    }
}